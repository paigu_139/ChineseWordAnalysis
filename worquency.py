#coding:utf-8  
import os
import sys  
reload(sys)  
sys.setdefaultencoding('utf8')  

from os import path  
import matplotlib.pyplot as plt  
from matplotlib.image import imread
import jieba  
  
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator  

from setting import *

  
stopwords = {}  
def importStopword(filename=''):  
    global stopwords  
    f = open(filename, 'r')
    line = f.readline().rstrip()  
  
    while line:  
        stopwords.setdefault(line, 0)  
        stopwords[line] = 1  
        line = f.readline().rstrip()  
  
    f.close()  
  
def processChinese(text):  
    # jieba.enable_parallel(4)    # 4核 windows不支持

    seg_generator = jieba.cut(text)  # 使用结巴分词，也可以不使用  
  
    seg_list = [i for i in seg_generator if i not in stopwords]  
  
    seg_list = [i for i in seg_list if len(i) > 1 and check_contain_chinese(i)]  
  
    # seg_list = r' '.join(seg_list)  

    word_freq = dict()
    for word in seg_list:
        if word_freq.has_key(word):
            word_freq[word] += 1
        else:
            word_freq[word] = 1

    save_word_freq(word_freq)
    return word_freq  

def check_contain_chinese(check_str):
    for ch in check_str.decode('utf-8'):
        if u'\u4e00' <= ch <= u'\u9fff':
            return True
    return False

def save_word_freq(word_freq):
    fo = open("word_freq.txt", "wb")
    word_freq = sorted(word_freq.items(), key=lambda d:d[1], reverse = True)
    for key,value in word_freq:
        fo.write(key + ',' + str(value) + "\r\n")
    fo.close()

def load_word_freq():
    fo = open("word_freq.txt", "r")
    word_freq = dict()
    for line in fo:
        line = line.decode('utf8')
        word_info = line.split(',')
        word_freq[word_info[0]] = int(word_info[1])
    fo.close()
    return word_freq
  
importStopword(filename='./stopwords.txt')  
  
  
# 获取当前文件路径  
# __file__ 为当前文件, 在ide中运行此行会报错,可改为  
# d = path.dirname('.')  
d = path.dirname(__file__)  

if os.path.exists('word_freq.txt'):
    word_freq = load_word_freq()
else:
    text = open(path.join(d, 'content.csv')).read()  
    #如果是中文  
    word_freq = processChinese(text)#中文不好分词，使用Jieba分词进行  
  


  
# read the mask / color image  
# taken from http://jirkavinse.deviantart.com/art/quot-Real-Life-quot-Alice-282261010  
# 设置背景图片  
back_coloring = imread(path.join(d, "./images/italy.jpg"))  

wc = WordCloud( font_path = font_path,#设置字体  
                background_color = background_color, #背景颜色  
                max_words = max_words,# 词云显示的最大词数  
                mask = back_coloring,#设置背景图片  
                max_font_size = max_font_size, #字体最大值  
                min_font_size = min_font_size,
                width = 1960,
                height = 1080,
                scale = scale,  # 图像精细程度
                random_state = random_state  # 配色方案(随机种子)
                )  
# 生成词云, 可以用generate输入全部文本(中文不好分词),也可以我们计算好词频后使用generate_from_frequencies函数  
# wc.generate(text)  
wc.generate_from_frequencies(word_freq)  
# txt_freq例子为[('词a', 100),('词b', 90),('词c', 80)]  
# 从背景图片生成颜色值  
# image_colors = ImageColorGenerator(back_coloring)  
# wc.recolor(color_func=image_colors)
  
plt.figure()  
# 以下代码显示图片  
plt.imshow(wc)  
plt.axis("off")  
plt.show()  
# 绘制词云  
  
# 保存图片  
wc.to_file(path.join(d, "result.png"))  
