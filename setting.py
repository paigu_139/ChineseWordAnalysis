#coding:utf-8  
font_path ='./cuyuan.ttf' #设置字体  
background_color = "black" #背景颜色  
max_words = 1000 # 词云显示的最大词数  
max_font_size = 200 #字体最大值  
min_font_size = 5 #字体最小值
scale = 4  # 图像精细程度 越大越精细 但是越慢
random_state = 2018  # 配色方案(随机种子)